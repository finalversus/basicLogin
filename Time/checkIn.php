<?php
require_once 'timer.php';

$data = file_get_contents('php://input');
$json = json_decode($data, true);
if($json['userId'] && $json['checkIn'] && $json['locationIn']){
  $userId = $json['userId'];
  $checkIn = $json['checkIn'];
  $locationIn = $json['locationIn'];

  $userObject = new User();
  $results = $userObject->checkIn($userId, $checkIn, $locationIn);
}else{
  $results = Array( "Code" => 1,
                "Message" => "Invalid information for check in.");
}
header("Content-Type: application/json");
echo json_encode($results);
 ?>
