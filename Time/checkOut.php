<?php
  require_once 'timer.php';

  $data = file_get_contents('php://input');
  $json = json_decode($data, true);
  if($json['userId'] && $json['checkIn'] && $json['checkOut']
    && $json['locationOut'] && $json['sumTimer']){
      $userId = $json['userId'];
      $checkIn = $json['checkIn'];
      $checkOut = $json['checkOut'];
      $locationOut = $json['locationOut'];
      $sumTimer = $json['sumTimer'];

      $userObject = new User();
      $results = $userObject->checkOut($userId, $checkIn, $checkOut,
        $locationOut, $sumTimer);
    }else {
      $results = Array("Code" => 1,
                    "Message" => "Invalid information for check out.");
    }
    header("Content-Type: application/json");
    echo json_encode($results);
 ?>
