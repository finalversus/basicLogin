<?php
  require_once 'timer.php';

  $data = file_get_contents('php://input');
  $json = json_decode($data, true);

  if ($json['sUsername'] && $json['sPassword']){
    $sUsername = $json['sUsername'];
    $sPassword = $json['sPassword'];
    $socialType = $json['socialType'];
    $socialId = $json['socialId'];
    $userObject = new User();
    $results = $userObject->loginUser($sUsername, $sPassword, $socialType, $socialId);
  } else {
    $results = Array("Code" => 1,
                  "Message" => "Username or Password can't be blank.");
  }
  header("Content-Type: application/json");
  echo json_encode($results);
 ?>
