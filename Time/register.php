<?php
require_once 'timer.php';

$data = file_get_contents('php://input');
$json = json_decode($data, true);
if($json['sUsername'] && $json['sPassword'] && $json['sName']
  && $json['sEmail'] && $json['personalId']){
    $sUsername = $json['sUsername'];
    $sPassword = $json['sPassword'];
    $sName = $json['sName'];
    $sEmail = $json['sEmail'];
    $personalId = $json['personalId'];
    $socialType = $json['socialType'];
    $socialId = $json['social_id'];

    $userObject = new User();
    $results = $userObject->createUser($sUsername, $sPassword, $sName,
      $sEmail, $personalId, $socialType, $socialId);
}else{
  $results = Array("Message" => "Invalid register information.",
                  "Code" => 1);
}
header("Content-Type: application/json");
echo json_encode($results);
 ?>
