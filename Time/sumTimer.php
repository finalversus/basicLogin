<?php
  require_once 'timer.php';

  $data = file_get_contents('php://input');
  $json = json_decode($data, true);
  if($json['userId'] || $json['sUsername']){
    $userId = $json['userId'];
    $sUsername = $json['sUsername'];

    $userObject = new User();
    $results = $userObject->sumTimer($userId, $sUsername);
  }else{
    $results = Array( "Code" => 1,
                  "Message" => "Please enter User ID or Username");
  }header("Content-Type: application/json");
  echo json_encode($results);
 ?>
