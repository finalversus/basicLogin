<?php

include_once 'db-connect.php';

class User {

  private $db;
  private $timer_user = "timer_user";
  private $timer_checking = "timer_checking";
  private $strCode = "Code";
  private $strMessage = "Message";

  public function __construct(){
    $this->db = new DbConnect();
  }

  public function loginUser($sUsername, $sPassword, $socialType, $socialId){

    if($socialType){
      $query = "select * from " . $this->timer_user
      . " where '$socialType' = '$socialId'";
    }else {
      $query = "select * from " . $this->timer_user
      . " where username = '$sUsername' and password = '$sPassword'";
    }
    $results = mysqli_query($this->db->getDb(), $query);
    if(mysqli_num_rows($results) > 0 ){
      $row = $results->fetch_assoc();
      $json = Array( 'sUsername' => $row["username"],
                  'userId' => $row['user_id'],
                  'sName' => $row['name'],
                  'sEmail' => $row['email'],
                  'personalId' => $row['personal_id'],
                  'createdTime' => $row['created'],
                  'facebookId' => $row['facebook'],
                  'googleId' => $row['google'],
                $this->strCode => 0,
                $this->strMessage => "Successfully, logged in");
    }else {
      $json = Array($this->strCode => 2,
                  $this->strMessage => "Invalid Username or Password.");
    }
    mysqli_close($this->db->getDb());
    return $json;
  }

  public function createUser($sUsername, $sPassword, $sName,
    $sEmail, $personalId, $socialType, $socialId){
    $isEmailUsernameExist = $this->isEmailUsernameExist($sUsername,$sEmail);
    if($isEmailUsernameExist['isExist']){
      return $isEmailUsernameExist;
    }
    if(!$socialType){
      $query = "insert into " . $this->timer_user
        . " values ('NULL', '$sUsername', '$sPassword', '$sName', '$sEmail', '$personalId', NOW(), 0, 0)";
    }elseif ($socialType=="facebook") {
      $query = "insert into " . $this->timer_user
        . " values ('NULL', '$sUsername', '$sPassword', '$sName', '$sEmail', '$personalId', NOW(), '$socialId', 0)";
    }else {
      $query = "insert into " . $this->timer_user
        . " values ('NULL', '$sUsername', '$sPassword', '$sName', '$sEmail', '$personalId', NOW(), 0, '$socialId')";
    }
    $inserted = mysqli_query($this->db->getDb(), $query);
    $json = array();
    if($inserted == 1){
      $json[$this->strCode] = 0;
      $json[$this->strMessage] = "Successfully, now you are registered";
    }else {
        $json[$this->strCode] = 2;
        $json[$this->strMessage] = "Error in registering";
    }
    mysqli_close($this->db->getDb());
    return $json;
  }

  public function isEmailUsernameExist($sUsername, $sEmail){
    $query = "select * from `" . $this->timer_user
      . "` where username = '$sUsername' or email = '$sEmail'";
    $results = mysqli_query($this->db->getDb(), $query);
    if(mysqli_num_rows($results) > 0 ){
      $json = Array('isExist' => true,
                  $this->strCode => 3,
                  $this->strMessage => "Username or Email is already used.");
      mysqli_close($this->db->getDb());
    }else {
      $json = Array('isExist'=>false);
    }
    return $json;
  }

  public function checkIn($userId, $checkIn, $locationIn){
    $query = "insert into `" . $this->timer_checking
    . "` (`user_id`, `check_in`, `location_in`) values ('$userId', '$checkIn', '$locationIn')";
    $inserted = mysqli_query($this->db->getDb(), $query);
    $json = array();
    if($inserted){
      $json[$this->strCode] = 0;
      $json[$this->strMessage] = "Check in complete.";
    }else{
      $json[$this->strCode] = 2;
      $json[$this->strMessage] = "Error. can't check in.";
    }
    mysqli_close($this->db->getDb());
    return $json;
  }

  public function checkOut($userId, $checkIn, $checkOut, $locationOut, $sumTimer){
    $query = "select * from `" . $this->timer_checking
    . "` where `user_id` = '$userId'";
    $results = mysqli_query($this->db->getDb(), $query);
    if(mysqli_num_rows($results) > 0 ){
      $query = "update `" . $this->timer_checking
      . "` set `check_out` = '$checkOut', `location_out` = '$locationOut', `sum_timer` = '$sumTimer'"
      . " where `user_id` = '$userId' and `check_in` = '$checkIn'";
      $updated = mysqli_query($this->db->getDb(), $query);
      $json[$this->strCode] = 0;
      $json[$this->strMessage] = "Check out complete.";
    }else {
      $json[$this->strCode] = 2;
      $json[$this->strMessage] = "Error, can't check out.";
    }
    mysqli_close($this->db->getDb());
    return $json;
  }

  public function sumTimer($userId, $sUsername){
    if($sUsername){
      $query = "select * from `" . $this->timer_user
      . "` where `username` = '$sUsername'";
      $results =  mysqli_query($this->db->getDb(), $query);
      if(mysqli_num_rows($results) > 0){
        $row = $results->fetch_assoc();
        $userId = $row['user_id'];
      }else {
        $json = Array( $this->strCode => 2,
                      $this->strMessage => "Username is wrong.");
        mysqli_close($this->db->getDb());
        return $json;
      }
    } $query = "select sum_timer from `" . $this->timer_checking
    . "` where `user_id`='$userId'";
    $results = mysqli_query($this->db->getDb(), $query);
    if(mysqli_num_rows($results) > 0) {
      $sum = 0;
      while ($row = $results->fetch_assoc()){
        $sum = $sum + $row['sum_timer'];
      }
    }else {
      $json = Array( $this->strCode => 2,
                    $this->strMessage => "User ID is wrong.",
                    'SumTimer' => 0);
      mysqli_close($this->db->getDb());
      return $json;
    }
    $json = Array($this->strCode => 0,
                $this->strMessage => "Calculated.",
                'SumTimer' => $sum);
    mysqli_close($this->db->getDb());
    return $json;
  }
}
