//
//  LoginViewController.swift
//  basicLogin
//
//  Created by Thepprasit Klinthanom on 12/22/16.
//  Copyright © 2016 ThepprasitKlinthanom. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    var myURL:String! = "http://fiew.esy.es/Timer/login.php";
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: true);
        self.hideKeyboardWhenTappedAround();
        NotificationCenter.default.addObserver(self, selector: #selector (LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        usernameField.delegate = self;
        usernameLabel.numberOfLines = 0;
        passwordField.delegate = self;
        passwordLabel.numberOfLines = 0;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func signInPressed(_ sender: Any) {
        let username = usernameField.text;
        let password = passwordField.text;
        var strMessage = "";
        var isCorrect: Bool = true;
        
        //        let defaluts = UserDefaults.standard;
        //        let usernameStored = defaluts.string(forKey: "username");
        //        let passwordStored = defaluts.string(forKey: "password");
        //
        //        if (usernameStored != username || passwordStored != password){
        //            MyAlertMessage(myMessage:"Username or Password invalid.");
        //            print("test printing");
        //        }else {
        ////            MyAlertSuccessful(myMessage: "Sign In Successful");
        //            let resultViewController = storyboard?.instantiateViewController(withIdentifier: "welcomeViewController") as! WelcomeViewController;
        //            self.present(resultViewController, animated: true, completion: nil);
        //
        //        }
        
        print("button pressed.");
        self.dismissKeyboard();
        if(!validUsername(strUsername: username!)){
            strMessage = "Invalid Username, it could be (0-9,a-z,A-Z,_) character.\n";
            isCorrect = false;
        }
        
        if(!validPasword(strPassword: password!)){
            strMessage = strMessage + "Invalid Password.\n"
            isCorrect = false;
        }
        
        if(isCorrect==true){
            loginAuthenSimple (sUsername:username!, sPassword:password!);
        }else{
            MyAlertMessage(myMessage: strMessage);
        }
        
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField){
        let usernameText = usernameField.text;
        let passwordText = passwordField.text;
        switch(textField){
        case usernameField :
            if(!validUsername(strUsername: usernameText!)){
                usernameLabel.isHidden = false;
                usernameLabel.text = "Invalid username, it's must be (0-9,a-z,A-Z,_) character.";
            }else{
                usernameLabel.isHidden = true;
            };
        break;
        case passwordField :
            if(!validPasword(strPassword: passwordText!)){
                passwordLabel.isHidden = false;
                passwordLabel.text = "Invalid password, It's must be (0-9,a-z,A-z,_) character.";
            }else{
                passwordLabel.isHidden = true;
            };
            break;
        default: break;
        }
    }
    
    func MyAlertMessage (myMessage:String){
        let myAlert = UIAlertController(title: "Sign In", message: myMessage, preferredStyle: UIAlertControllerStyle.alert);
        let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil);
        
        myAlert.addAction(actionOk);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    func MyAlertSuccessful (myMessage:String){
        let resultViewController = storyboard?.instantiateViewController(withIdentifier: "welcomeViewController:") as! WelcomeViewController;
        let myAlert = UIAlertController(title: "Successful", message: myMessage, preferredStyle: UIAlertControllerStyle.alert);
        let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            actionOk in self.present(resultViewController, animated: true, completion: nil);
        }
        
        myAlert.addAction(actionOk);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    func validUsername(strUsername:String) -> Bool{
        let usernameRegEx = "^(\\w){2,11}$";
        let usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegEx);
        return usernameTest.evaluate(with: strUsername);
    }
    
    func validPasword(strPassword:String) -> Bool{
        let passwordRegEx = "^(\\w){3,11}$";
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx);
        return passwordTest.evaluate(with: strPassword);
    }
    
    func loginAuthenSimple (sUsername:String, sPassword:String){
        
        
        // create JSON data
        let json = [ "sUsername" : sUsername , "sPassword" : sPassword ];
        print(json);
        do{
            
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted);
            
            // POST request
            let POSTURL = NSURL(string: myURL);
            let request = NSMutableURLRequest(url:POSTURL! as URL);
            request.httpMethod = "POST";
            
            //insert JSONData, create task to send data to server
            request.httpBody = jsonData;
            request.addValue("application/json", forHTTPHeaderField: "Content-Type");
            request.addValue("application/json", forHTTPHeaderField: "Accept");
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler:{ data, response, error in
                guard error == nil else{
                    return;
                }
                guard let data = data else{
                    return;
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] {
                        //handle JSON
                        print(json);
                        let successResult = json["Code"] as! Int;
                        if(successResult==0){
                            
                            let defaults = UserDefaults.standard;
                            let userId = Int(json["userId"] as! String);
                            defaults.set(userId, forKey: "userId");
                            defaults.synchronize();
                            OperationQueue.main.addOperation {
                                self.goToWelcomeView();
                            }
                        }else{
                            let messageResult = json["Message"] as! String;
                            OperationQueue.main.addOperation {
                                self.MyAlertMessage(myMessage: messageResult);
                            }
                        }
                        print("Result :", terminator: " ");
                        print(successResult);
                        
                    }
                } catch let error{
                    
                    print(error.localizedDescription);
                }
            });
            
            task.resume();
        } catch let error{
            
            print(error.localizedDescription);
        }
    }
    
    func goToWelcomeView(){
        let resultViewController = storyboard?.instantiateViewController(withIdentifier: "welcomeViewController") as? WelcomeViewController;
        self.view.window?.rootViewController = resultViewController;
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 90;
            }
//        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 90;
            }
//        }
    }
    
}


