//
//  RegisterViewController.swift
//  basicLogin
//
//  Created by Thepprasit Klinthanom on 12/26/16.
//  Copyright © 2016 ThepprasitKlinthanom. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {

   
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var scrollViewController: UIScrollView!
    
    @IBOutlet weak var passwordRegisterField: UITextField!
    @IBOutlet weak var conPasswordRegisterField: UITextField!
    @IBOutlet weak var nameRegisterField: UITextField!
    @IBOutlet weak var surnameRegisterField: UITextField!
    @IBOutlet weak var usernameRegisterField: UITextField!
    @IBOutlet weak var emailRegisterField: UITextField!
    @IBOutlet weak var persoanlIdRegisterField: UITextField!
    @IBOutlet weak var phoneRegisterField: UITextField!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var conPasswordLabel: UILabel!
    @IBOutlet weak var personalIdLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var signUpBtn: UIButton!
    
    var myURL: String! = "http://fiew.esy.es/Timer/register.php";
    var strMessage = "";
    var isCorrect: Bool! = false;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        scrollViewController.contentSize.height = signUpBtn.frame.origin.y + 80;
        // Do any additional setup after loading the view.
        
        self.navigationController?.setNavigationBarHidden(false, animated: true);
        self.hideKeyboardWhenTappedAround();
        NotificationCenter.default.addObserver(self, selector: #selector (LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        nameRegisterField.delegate = self;
        surnameRegisterField.delegate = self;
        usernameRegisterField.delegate = self;
        emailRegisterField.delegate = self;
        persoanlIdRegisterField.delegate = self;
        passwordRegisterField.delegate = self;
        phoneRegisterField.delegate = self;
        conPasswordRegisterField.delegate = self;
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        _ = navigationController?.popViewController(animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        let name = nameRegisterField.text;
        let surname = surnameRegisterField.text;
        let username = usernameRegisterField.text;
        let email = emailRegisterField.text;
        let personalId = persoanlIdRegisterField.text;
        let phoneNumber = phoneRegisterField.text;
        let password = passwordRegisterField.text;
        let conPassword = conPasswordRegisterField.text;
        
        switch(textField){
        case nameRegisterField :
            print("name case")
            if(!isAlphabet(strAlphabet: name!)){
                strMessage = "Invalid name.\n";
                isCorrect = false;
                nameLabel.isHidden = false;
                nameLabel.text = "Invalid Name.";
            }else {
                isCorrect = true;
                nameLabel.isHidden = true;
            };
            break;
            
        case surnameRegisterField :
            print("surname case")
            if(!isAlphabet(strAlphabet: surname!)){
                strMessage = strMessage + "Invalid surname.\n";
                isCorrect = false;
                surnameLabel.isHidden = false;
                surnameLabel.text = "Invalid Surname.";
            }else {
                isCorrect = true;
                surnameLabel.isHidden = true;
            };
            break;
            
        case usernameRegisterField :
            print("username case")
            if(!isUsername(strUsername: username!)){
                strMessage = strMessage + "Invalid username, it could be (0-9,a-z,A-Z,_) character.\n";
                isCorrect = false;
                usernameLabel.isHidden = false;
                usernameLabel.text = "Invalid Username.";
            }else {
                isCorrect = true;
                usernameLabel.isHidden = true;
            };
            break;
            
        case emailRegisterField :
            print("email case")
            if(!isValidEmail(validEmail: email!)){
                strMessage = strMessage + "Invalid Email.\n";
                isCorrect = false;
                emailLabel.isHidden = false;
                emailLabel.text = "Invalid Email.";
            }else {
                isCorrect = true;
                emailLabel.isHidden = true;
            }
            break;
        case persoanlIdRegisterField :
            print("id case")
            if(!isPersonalId(strPersonalId: personalId!)){
                strMessage = strMessage + "Personal Id must be only 13 character.\n";
                isCorrect = false;
                personalIdLabel.isHidden = false;
                personalIdLabel.text = "Personal Id must be only 13 character.";
            }else {
                isCorrect = true;
                personalIdLabel.isHidden = true;
            };
            break;
        
        case phoneRegisterField :
            print("phone case")
            if(!isPhoneNumber(strNumber: phoneNumber!)){
                strMessage = strMessage + "Invalid phone number.\n"
                isCorrect = false;
                phoneLabel.isHidden = false;
                phoneLabel.text = "Invalid Phone number."
            }else {
                isCorrect = true;
                phoneLabel.isHidden = true
            }

        case passwordRegisterField :
            print ("pass case");
            if(!isPassword(strPassword: password!)){
                strMessage  = strMessage + "Invalid Password.\n";
                isCorrect = false;
                passwordLabel.isHidden = false;
                passwordLabel.text = "Invalid Password.";
            }else{
                isCorrect = true;
                passwordLabel.isHidden = true;
            }
            break;
            
        case conPasswordRegisterField :
            print("conpass case")
            if(password != conPassword){
                strMessage = strMessage + "Password is not match.\n"
                isCorrect = false;
                conPasswordLabel.isHidden = false;
                conPasswordLabel.text = "Password is not match.";
            }else {
                isCorrect = true;
                conPasswordLabel.isHidden = true;
            };
            break;
        
                default :
            print("something wrong")
            break;
            
        }
    }
    
    @IBAction func btnSignUpPressed(_ sender: Any) {
        let name = nameRegisterField.text;
        let surname = surnameRegisterField.text;
        let username = usernameRegisterField.text;
        let email = emailRegisterField.text;
        let personalId = persoanlIdRegisterField.text;
        let password = passwordRegisterField.text;
        
        let fullName = name!+" "+surname!;

        if(isCorrect==true){
            let json = [ "sName" : fullName, "sUsername" : username, "sEmail" : email, "personalId" : personalId, "sPassword" : password];
            print(json);
            do{
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted);
                
                //POST request.
                let POSTURL = NSURL(string: myURL);
                let request = NSMutableURLRequest(url: POSTURL! as URL);
                request.httpMethod = "POST";
                
                //Insert JSONData.
                request.httpBody = jsonData;
                request.addValue("application/json", forHTTPHeaderField: "Content-Type");
                request.addValue("application/json", forHTTPHeaderField: "Accept");
                
                let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                    guard error == nil else{
                        return;
                    }
                    guard let data = data else{
                        return;
                    }
                    do{
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]{
                            //handle json.
                            print (json);
                            let result = json["Code"] as? Int;
                            if(result==0){
                                let jsonMessage = json["Message"] as? String;
                                OperationQueue.main.addOperation {
                                    self.successAlert(strMessage: jsonMessage!);
                                }
                            }else{
                                let jsonMessage = json["Message"] as? String;
                                OperationQueue.main.addOperation {
                                    self.registerAlert(strMessage: jsonMessage!);
                                }
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription);
                    }
                    
                    
                });
                task.resume();
            } catch let error {
                print(error.localizedDescription);
            }

        }else{
            registerAlert(strMessage: "Invalid information for register.");
        }
        
    }
    
    func isValidEmail(validEmail:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: validEmail)
    }
    
    func isAlphabet(strAlphabet:String) -> Bool {
        let alphabetRegEx = "^([a-z]|[A-Z]){3,}$";
        let alphabetTest = NSPredicate(format: "SELF MATCHES %@", alphabetRegEx);
        return alphabetTest.evaluate(with: strAlphabet);
    }
    
    func isPersonalId(strPersonalId:String) -> Bool{
        let personalIdRegEx = "^(\\d){13}$";
        let personalIdTest = NSPredicate(format: "SELF MATCHES %@", personalIdRegEx);
        return personalIdTest.evaluate(with: strPersonalId);
    }
    
    func isUsername(strUsername:String) -> Bool {
        let usernameRegEx = "^(\\w){3,11}$";
        let usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegEx);
        return usernameTest.evaluate(with: strUsername);
    }
    
    func isPassword(strPassword:String) -> Bool {
        let passwordRegEx = "^(\\w){3,11}$";
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx);
        return passwordTest.evaluate(with: strPassword);
    }
    func isPhoneNumber(strNumber:String) -> Bool {
        let numberRegEx = "^0{1}\\d{9}$";
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx);
        return numberTest.evaluate(with: strNumber);
    }
    
    func registerAlert(strMessage:String){
        let regAlert = UIAlertController(title: "Sign Up", message: strMessage, preferredStyle: UIAlertControllerStyle.alert);
        let actionOk = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: nil);
        regAlert.addAction(actionOk);
        self.present(regAlert, animated: true, completion: nil);
    }
    
    func successAlert (strMessage:String){
        let myAlert = UIAlertController(title: "Sign Up", message: strMessage, preferredStyle: UIAlertControllerStyle.alert);
        let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            actionOk in self.viewWillDisappear(true);
        }
        myAlert.addAction(actionOk);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let signUpY = self.signUpLabel.frame.origin.y;
        let keyboardSizeH = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size;
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(signUpY, 0.0, keyboardSizeH!.height, 0.0)
        self.scrollViewController.isScrollEnabled = true
        self.scrollViewController.contentInset = contentInsets
        self.scrollViewController.scrollIndicatorInsets = contentInsets
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let signUpY = self.signUpLabel.frame.origin.y;
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(signUpY, 0.0, 0.0, 0.0);
        self.scrollViewController.contentInset = contentInsets
        self.scrollViewController.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollViewController.isScrollEnabled = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}
