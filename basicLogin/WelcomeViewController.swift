//
//  WelcomeViewController.swift
//  basicLogin
//
//  Created by Thepprasit Klinthanom on 12/23/16.
//  Copyright © 2016 ThepprasitKlinthanom. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var timerCheckerBtn: UIButton!
    @IBOutlet weak var checkInBtn: UIButton!
    var timer = Timer()
    var dataString :String = "";
    var timeCheckIn :String = "";
    var timeCheckOut :String = "";
    var checkInPoint: Int = 0;
    var checkOutPoint: Int = 0;
    var dateIn: Date = NSDate() as Date;
    var URLCheckIn: String! = "http://fiew.esy.es/Timer/checkIn.php";
    var URLCheckOut: String! = "http://fiew.esy.es/Timer/checkOut.php";
    var URLSumTimer: String! = "http://fiew.esy.es/Timer/sumTimer.php";
    var isCheckIn: Bool! = true;
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(WelcomeViewController.setTime), userInfo: nil, repeats: true);
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTime(){
        let date: NSDate = NSDate();
        let dateFormat: DateFormatter  = DateFormatter();
//        dateFormat.setLocalizedDateFormatFromTemplate("Y-M-d HH:mm:ss");
        dateFormat.dateFormat = "Y-M-d HH:mm:ss";
        dataString = dateFormat.string(from: date as Date);
        currentTimeLabel.text = dataString as String;
        
//        currentTimeLabel.text = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .medium);
    }

    
    @IBAction func CheckInBtnPressed(_ sender: Any) {
        currentTimeLabel.isHidden = false
        //Set time for send.
        var myURL:String = URLCheckIn;
        let currentTime = currentTimeLabel.text;
        var point:Int = 0;
        var currentLocation = "A";
        let defaults = UserDefaults.standard;
        let currentUser = defaults.integer(forKey: "userId");
        
        if isCheckIn! {
            isCheckIn = false;
            checkInBtn.setTitle("Check Out", for: .normal);
            checkInBtn.backgroundColor = UIColor(red:48/225, green:119/225, blue:167/225, alpha:1);
            checkInBtn.setTitleColor(UIColor.white, for: .normal);
            timeCheckIn = currentTimeLabel.text!;
            
            
            let date = NSDate() as Date;
            checkInPoint = Int(date.timeIntervalSinceReferenceDate);
            dateIn = date;
        }else {
            isCheckIn = true;
            checkInBtn.setTitle("Check In", for: .normal);
            checkInBtn.backgroundColor = UIColor.white;
            checkInBtn.setTitleColor(UIColor(red:48/225, green:119/225, blue:167/225, alpha:1), for: .normal);
            myURL = URLCheckOut;
            currentLocation = "B";
            timeCheckOut = currentTimeLabel.text!;
            
            let date = NSDate() as Date;
            checkOutPoint = Int(date.timeIntervalSinceReferenceDate);
            var different = (checkOutPoint - checkInPoint);
            different = different / 1;
            let hour = different/3600;
            if (different/60 >= 30){
                point = hour + 1;
            }else{
                point = hour;
            }
            
        }
        checking(currentUser:currentUser, currentTime: currentTime! , currentLocation:currentLocation, point:point, myURL:myURL);
    }
    
    func checking(currentUser:Int, currentTime:String, currentLocation:String, point:Int, myURL:String){
        //POST check in or check out.
        var json:[String:Any];
        if !isCheckIn {
            json = ["userId" : currentUser, "checkIn" : currentTime, "locationIn" : currentLocation] as [String : Any];
        }else{
            json = ["userId" : currentUser,"checkIn" : timeCheckIn, "checkOut" : currentTime, "locationOut" : currentLocation, "sumTimer" : point] as [String:Any];
        }
        
        print (json);
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted);
            
            //POST request.
            let POSTURL = NSURL(string: myURL);
            let request = NSMutableURLRequest(url: POSTURL! as URL);
            request.httpMethod = "POST";
            
            //insert JSONData.
            request.httpBody = jsonData;
            request.addValue("application/json", forHTTPHeaderField: "Content-Type");
            request.addValue("application/json", forHTTPHeaderField: "Accept");
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
               data, response, error in
                guard error == nil else{
                    return;
                }
                guard let data = data else{
                    return;
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] {
                        //handle JSON.
                        print(json);
                        
                    }else {
                        
                    }
                } catch let error{
                    print(error.localizedDescription);
                }
            });
            task.resume();
        } catch let error{
            print(error.localizedDescription);
        }
    }
    
    
    @IBAction func sumTimer(_ sender: Any) {
        let defaults = UserDefaults.standard;
        let currentUser = defaults.integer(forKey: "userId");
        let json = ["userId" : currentUser
        ] as [String:Any];
        print (json);
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted);
            
            //POST request.
            let POSTURL = NSURL(string: URLSumTimer);
            let request = NSMutableURLRequest(url: POSTURL! as URL);
            request.httpMethod = "POST";
            
            //insert JSONData.
            request.httpBody = jsonData;
            request.addValue("application/json", forHTTPHeaderField: "Content-Type");
            request.addValue("application/json", forHTTPHeaderField: "Accept");
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
                data, response, error in
                guard error == nil else{
                    return;
                }
                guard let data = data else{
                    return;
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] {
                        //handle JSON.
                        print(json);
                        let strMessage = json["SumTimer"] as! Int;
                        let NSInt = strMessage as NSNumber;
                        var message: String = NSInt.stringValue;
                        
                        OperationQueue.main.addOperation {
                            message = message + " hours"
                            self.MyAlertMessage(myMessage: message);
                        }
                    }else {
                        
                    }
                } catch let error{
                    print(error.localizedDescription);
                }
            });
            task.resume();
        } catch let error{
            print(error.localizedDescription);
        }
    
    }
    
    
    func MyAlertMessage (myMessage:String){
        let myAlert = UIAlertController(title: "Total Time", message: myMessage, preferredStyle: UIAlertControllerStyle.alert);
        let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil);
        
        myAlert.addAction(actionOk);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
