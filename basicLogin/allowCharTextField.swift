//
//  allowCharTextField.swift
//  basicLogin
//
//  Created by Thepprasit Klinthanom on 12/29/16.
//  Copyright © 2016 ThepprasitKlinthanom. All rights reserved.
//

import UIKit
import Foundation

class allowCharTextField: UITextField, UITextFieldDelegate {
    
    
    @IBInspectable var allowedChars: String = "";
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
        // 2
        autocorrectionType = .no
    }
    
    func allowedIntoTextField(text: String) -> Bool {
        return text.containsOnlyCharactersIn(matchCharacters: allowedChars);
    }
        
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

private extension String {
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = CharacterSet(charactersIn: matchCharacters).inverted;
        return (self.rangeOfCharacter(from: disallowedCharacterSet) != nil);
    }
    
}
