<?php

include_once 'db-connect.php';

class User {

    private $db;
    private $db_table = "table1";
    private $db_table_time = "table2";

    public function __construct() {
        $this->db = new DbConnect();
    }

    public function isLoginExist($user_name, $password) {
        $query = "select * from " . $this->db_table . " where user_name = '$user_name' and password = '$password'";
        $result = mysqli_query($this->db->getDb(), $query);
        if (mysqli_num_rows($result) > 0) {
            mysqli_close($this->db->getDb());

            $row = $result->fetch_assoc();
            $json = Array('can'=>TRUE,
                        'username'=>$row["user_name"]);
            return $json;
        }
        mysqli_close($this->db->getDb());
        $json = Array('can'=>FALSE);
        return $json;
    }
    
    public function isSynced($username, $password) {
        $query = "select * from `" . $this->db_table . "` where `facebook` = '$password' OR `google_plus` = '$password' OR `twitter` = '$password'";
        $result = mysqli_query($this->db->getDb(), $query);
        if (mysqli_num_rows($result) > 0) {
            $row = $result->fetch_assoc();
            $json = Array('synced'=>TRUE,
                        'username'=>$row["user_name"]);
            mysqli_close($this->db->getDb());

            return $json;
        }
//        mysqli_close($this->db->getDb());
        $json = Array('synced'=>FALSE);
        return $json;
    }

    public function loginUsers($username, $password) {

        $json = array();
        $socialSynced = $this->isSynced($username, $password);
        if(!$socialSynced["synced"]){
            $canUserLogin = $this->isLoginExist($username, $password);
            $useridname = $canUserLogin["username"];
        }else {
            $canUserLogin = Array('can'=>TRUE);
            $useridname = $socialSynced["username"];
        }
        if ($canUserLogin["can"]) {
            $json['username'] = $socialSynced["username"];
            $json['success'] = 1;
            $json['message'] = "Successfully logged in";
        } else {
            $json['success'] = 0;
            $json['message'] = "Incorrect details";
        }
        return $json;
    }

    public function createNewRegisterUser($user_name, $password) {

        $query = "insert into " . $this->db_table . " (user_id, user_name, password) values ('NULL', '$user_name', '$password')";
        $inserted = mysqli_query($this->db->getDb(), $query);

        if ($inserted == 1) {
            $json['success'] = 1;
            $json['message'] = "Successfully registered the user";
        } else {
            $json['success'] = 0;
            $json['message'] = "Error in registering. Probably the username/email already exists";
        }
        mysqli_close($this->db->getDb());
        return $json;
    }

    public function syncGoogleAccout($user_name, $social_type, $social_id) {
        $query = "UPDATE `" . $this->db_table . "` SET `" . $social_type . "` = '$social_id' WHERE `user_name` = '$user_name'";
        $updated = mysqli_query($this->db->getDb(), $query);

        if ($updated) {

            $json['success'] = 1;
            $json['message'] = "Successfully synced to " . $social_type;
        } else {
            $json['success'] = 0;
            $json['message'] = "Error to syncing";
        }
        mysqli_close($this->db->getDb());
        return $json;
    }

    public function checkOut($user_name, $check_in_time, $check_out_time, $pointage) {
        $query = "UPDATE `" . $this->db_table_time . "` SET `check_out` = '$check_out_time', `pointage` = '$pointage' "
                . "WHERE `user_name` = '$user_name' AND `check_in` = '$check_in_time'";
        $updated = mysqli_query($this->db->getDb(), $query);

        if ($updated) {
            $json['success'] = 1;
            $json['message'] = "Successfully. Check-out complete.";
        } else {
            $json['success'] = 0;
            $json['message'] = "Error. Can't Check-out.";
        }
        mysqli_close($this->db->getDb());
        return $json;
    }

    public function checkIn($user_name, $check_in_time) {
        $query = "INSERT INTO `" . $this->db_table_time . "` (user_name, check_in) values ('$user_name', '$check_in_time')";
        $inserted = mysqli_query($this->db->getDb(), $query);

        if ($inserted) {
            $json['success'] = 1;
            $json['message'] = "Successfully. Check-in complete.";
        } else {
            $json['success'] = 0;
            $json['message'] = "Error. Can't Check-in.";
        }
        mysqli_close($this->db->getDb());
        return $json;
    }

    

//    public function isEmailUsernameExist($username, $email) {
//
//        $query = "select * from " . $this->db_table . " where username = '$username' AND email = '$email'";
//        $result = mysqli_query($this->db->getDb(), $query);
//        if (mysqli_num_rows($result) > 0) {
//            mysqli_close($this->db->getDb());
//            return true;
//        }return false;
//    }
}

?>